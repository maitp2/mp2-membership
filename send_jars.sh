#!/bin/bash
# This bash will send Client and server jars files to all VMs
upperlim=10

for ((i=1; i<=upperlim; i++)); do
        echo "Sending jar file to VM $i"
        if [ "$i" -lt 10 ]; then
          scp -r out/artifacts/ maitp2@fa22-cs425-520$i.cs.illinois.edu:/home/maitp2/
        else
          scp -r out/artifacts/ maitp2@fa22-cs425-52$i.cs.illinois.edu:/home/maitp2/
        fi
done

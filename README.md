# CS425_MP2

### Mai Pham (maitp2) and Trang Do(trangtd2)

## Instructions

### Config
When running, the logdir in class ```src/main/java/cs425/mp2/Constant.java```, that means user need to create ./log/ dir

### Run Introducer
```
java -jar out/artifacts/mp2_membership_jar/mp2-membership.jar -ih <introducer_host> -ip <introducer_port> -i true
```
Example:
```
java -jar out/artifacts/mp2_membership_jar/mp2-membership.jar -ih localhost -ip 5000 -i true
```

### Run Process
```
java -jar out/artifacts/mp2_membership_jar/mp2-membership.jar -ih <introducer_host> -ip <introducer_port> -p <run_port> -i false" 
```
Example
```
java -jar artifacts/mp2_membership_jar/mp2-membership.jar -ih fa22-cs425-5201.cs.illinois.edu -ip 5000 -p 5002 -i false
```

### Note
After running process (except Introducer), run ```join``` to join membership
1. ``` join```: to join membership
2. ```leave```: to leave membership
3. ```members```: to print all member
4. ```id```: to print process id

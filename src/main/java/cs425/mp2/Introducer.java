package cs425.mp2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

public class Introducer extends Member {
    private Socket socket = null;
    private ServerSocket introServer = null;

    public Introducer(int port, String introHost, int introPort) throws UnknownHostException {
        super(port, true, introHost, introPort);
        this.memberList.add(this.id);
        this.status.set(Status.IN_MEMBERLIST);
        this.receiver = new Receiver(memberInfo);
    }

    @Override
    public void start() {
        logger.info("This is Introducer");
        logger.info("Introducer " + memberInfo.getHost() + " started");
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                handleNewMember();
            }
        });
        super.start();
    }

    @Override
    protected void joinMembership() {
        this.memberList.add(this.memberInfo.getID());
        logger.info("Introducer has joined!");
    }

    @Override
    protected void leaveMembership() {
        memberList.remove(this.memberInfo.getID());
        this.status.set(Status.NOT_IN_MEMBERLIST);
        receiver.close();
        logger.info("Introducer has left!");
    }

    private void handleNewMember() {
        try {
            int port = this.memberInfo.getPort();
            introServer = new ServerSocket(port);
            logger.info("Introducer start listening for new member on port " + port);

            while (true) {
                socket = introServer.accept();
                /*
                New member send its ID to introducer and introducer receive it and send back list members.
                 */
                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                try {
                    String mess = (String) inputStream.readObject();
                    String memSetStr = String.join("\n", memberList);

                    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                    outputStream.writeObject(memSetStr);

                    logger.info("New joined member successfully added");
                } catch (IOException | ClassNotFoundException e) {
                    System.err.println("Error reading message");
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            System.err.println("Cannot establish server socker on introducer side");
        }
    }
}

package cs425.mp2;

import java.util.concurrent.atomic.AtomicReference;

public enum Status {
    IN_MEMBERLIST,
    NOT_IN_MEMBERLIST,
    NEW_JOIN
}

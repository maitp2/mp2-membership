package cs425.mp2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Arrays;

public class Receiver {
    private MemberInfo receiverInfo;
    private DatagramSocket socket;

    public Receiver(MemberInfo receiverInfo) {
        this.receiverInfo = receiverInfo;
        try {
            this.socket = new DatagramSocket(receiverInfo.getPort());
        } catch (SocketException e) {
            System.err.println("Error creating UDP socket for receiver");
        }
    }

    public MessageInfo receive() {
        byte[] mes = new byte[65535];
        DatagramPacket revPacket = new DatagramPacket(mes, mes.length);
        try {
            socket.receive(revPacket);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Error receiving udp mess");
        }
        return MessageInfo.fromString(data(mes).toString());
    }

    public static StringBuilder data(byte[] a)
    {
        if (a == null)
            return null;
        StringBuilder ret = new StringBuilder();
        int i = 0;
        while (a[i] != 0)
        {
            ret.append((char) a[i]);
            i++;
        }
        return ret;
    }

    public void close() {
        socket.close();

    }

    public void reuseSocket() {
        if (!socket.isClosed()) return;
        try {
//            this.socket.bind(new InetSocketAddress(this.receiverInfo.getPort()));
            this.socket = new DatagramSocket(receiverInfo.getPort());
        } catch (SocketException e) {
            e.printStackTrace();
        }

    }

    public boolean isSocketClose() {
        return socket.isClosed();
    }

}

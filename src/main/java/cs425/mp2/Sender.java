package cs425.mp2;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;
import java.util.Random;

public class Sender {
    private MemberInfo senderInfo;
//    private DatagramSocket socket;

    public Sender(MemberInfo senderInfo) {
        this.senderInfo = senderInfo;

    }

    public void send(String receiverID, MessageType messageType, String targetMemID) {
        MemberInfo receiver = MemberInfo.fromString(receiverID);
        if (targetMemID == null || targetMemID.equals(" ")) {
            ping(receiver, " ");
            return;
        }

        MemberInfo targetMem = MemberInfo.fromString(targetMemID);

        switch (messageType) {
            case PING:
                ping(receiver, " "); // Info of this mess in empty string
                break;
            case NEW_JOIN:
                newJoin(receiver, targetMem); // Info of this mess is ID of target member
                break;
            case LEAVE_FAIL:
                leaveFail(receiver, targetMem); // Info of this mess is ID of target member
                break;
            default:
                System.err.println("Error recognizing messageType");
        }
    }

    public void newJoin(MemberInfo receiver, MemberInfo newJoinMem) {
        MessageInfo messageInfo = new MessageInfo(MessageType.NEW_JOIN, senderInfo.getID(), newJoinMem.getID());
        sendMessage(receiver, messageInfo.toString());
    }

    public void leaveFail(MemberInfo receiver, MemberInfo leaveFailMem) {
        MessageInfo messageInfo = new MessageInfo(MessageType.LEAVE_FAIL, senderInfo.getID(), leaveFailMem.getID());
        sendMessage(receiver, messageInfo.toString());
    }


    public void ping(MemberInfo receiver, String info) {
        MessageInfo messageInfo = new MessageInfo(MessageType.PING, senderInfo.getID(), info);
        sendMessage(receiver, messageInfo.toString());
    }

    private void sendMessage(MemberInfo receiver, String mes) {
//        double random = Math.random();
//        if (random <= 0.03) {
//            return;
//        }
        try {
//            System.out.println("debug " + receiver.getHost());
//            System.out.println("debug" + mes);
            DatagramSocket socket = new DatagramSocket();
            InetAddress address = InetAddress.getByName(receiver.getHost());
            byte[] message = mes.getBytes();
            DatagramPacket packet = new DatagramPacket(message, message.length, address, receiver.getPort());
            socket.send(packet);
            socket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

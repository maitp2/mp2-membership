package cs425.mp2;

enum MessageType {
    PING,
    ACK,
    NEW_JOIN,
    LEAVE_FAIL
}


package cs425.mp2;

public class MessageInfo {
    private MessageType messageType;
    private String memID; // info of the sender
    private String info;

    public MessageInfo(MessageType messageType, String memID, String info) {
        this.messageType = messageType;
        this.memID = memID;
        this.info = info;
    }

    public MessageInfo(MessageType messageType, String info) {
        this.messageType = messageType;
        this.info = info;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public String getMemID() {
        return memID;
    }

    public String getInfo() {
        return info;
    }

    public String toString() {
        return messageType + "\n" + memID + "\n" + info;
    }

    public void setMemID(String memID) {
        this.memID = memID;
    }

    public static MessageInfo fromString(String s) {
        String[] ss = s.split("\n");
        return new MessageInfo(
                MessageType.valueOf(ss[0]),
                ss[1],
                ss[2]
        );
    }
}

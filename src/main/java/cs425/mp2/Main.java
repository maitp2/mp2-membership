package cs425.mp2;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.net.UnknownHostException;

public class Main {
    public static void main(String[] args) {
        /*
        Arguments of members including:
        - ih: introducer host
        - ip: introducer port
        - p: member port
        - i: is Introducer
         */
        ArgumentParser parser = ArgumentParsers.newArgumentParser("Main")
                .defaultHelp(true)
                .description("Run member to join membership list");
        parser.addArgument("-ih","--introHost").nargs("?")
                .help("Host of introducer");
        parser.addArgument("-ip","--introPort").nargs("?")
                .help("port of introducer");
        parser.addArgument("-p","--port").required(false).nargs("?")
                .setDefault("5000")
                .help("Port to run member");
        parser.addArgument("-i","--isIntroducer").required(false).nargs("?")
                .setDefault(false)
                .help("Is this member an introducer");

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        /*
        Extract arguments and start member and introducer
         */
        int port = Integer.parseInt(ns.getString("port"));
        boolean isIntroducer = Boolean.parseBoolean(ns.getString("isIntroducer").toLowerCase());
        String introHost = ns.getString("introHost");
        int introPort = Integer.parseInt(ns.getString("introPort"));

        Member member;
        try {
            if (isIntroducer) {
                member = new Introducer(introPort, introHost, introPort);
                member.start();
            } else {
                member = new Member(port, false, introHost, introPort);
                member.start();
            }
        } catch (UnknownHostException e) {
            System.out.println("Cannot find host for this member");
            throw new RuntimeException(e);
        }

    }
}
package cs425.mp2;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class MemberInfo {
    private int port;
    private String host;
    private long timestamp;

    public MemberInfo(int port) throws UnknownHostException{
        this.port = port;
        this.host = InetAddress.getLocalHost().getHostName();
        this.timestamp = System.currentTimeMillis();
    }

    public MemberInfo(long timestamp, String host, int port) {
        this.port = port;
        this.host = host;
        this.timestamp = timestamp;
    }

    public int getPort() {
        return port;
    }

    public String getHost() {
        return host;
    }

    public String getID() {
        return timestamp +
                "::" +
                this.host +
                "::" +
                port;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public static MemberInfo fromString(String s) {
//        if (s.isEmpty()) return null;
        String[] ss = s.split("::");
        return new MemberInfo(Long.parseLong(ss[0]), ss[1], Integer.parseInt(ss[2]));
    }
}
